# My expectations for this course
* a relaxed and productive atmosphere 
* learning more about how to combine humanities and coding especially considering languages
* learning how to use fitting applications and software for projects 
* interesting tasks with practical value

### And now a short intermission 
[click me] (https://www.youtube.com/watch?v=rJfLHedBG1g)
